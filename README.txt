----------------------------------------------------------------------------
                     D R U P A L    M O D U L E
----------------------------------------------------------------------------
Name: Custom Operations
Author: Andrey Vitushkin <andrey.vitushkin at gmail.com>
Drupal: 8.x


INTRODUCTION
------------
The module demonstrates of creation custom operations links.
In a view will be created two additional dropbuttons: 'Publish' and 'Unpublish'.
After click on them a view will be updated via Ajax.


REQUIREMENTS
------------
This module does not requires additional modules.


INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
For further information visit:
https://www.drupal.org/docs/7/extend/installing-modules


CONFIGURATION
-------------
The module has no menu or modifiable settings.
There is no configuration.


HOW TO USE
------------
To see the module's job do the following:

1. Create a view to display a content.

2. Add 'Content: Published' and 'Content: Operations links' fields.

3. Open 'ADVANCED' section of view's settings and set 'Use AJAX: Yes'.

4. Specify a view's selector for refreshing a view with Ajax.
It can by a view's machine name, but underscores replaced with hyphens.
Also it can be a CSS class that you specified in 'ADVANCED' section
of a view's settings page.
Edit 'OperationsLinksController.php' file and replace the following string:
$selector = '.YOUR-VIEW-NAME';
with your view's selector.
