<?php

namespace Drupal\custom_operations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;

/**
 * Controller responses to Publish and Unpublish action links.
 */
class OperationsLinksController extends ControllerBase {

  /**
   * Publich a content.
   *
   * @param Drupal\node\Entity\Node $node
   *   The node entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   Refresh a view via Ajax.
   */
  public function publish(Node $node, Request $request) {

    $node->setPublished(TRUE);
    $node->save();

    return $this->generateResponse();
  }

  /**
   * Unpublich a content.
   *
   * @param Drupal\node\Entity\Node $node
   *   The node entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   Refresh a view via Ajax.
   */
  public function unpublish(Node $node, Request $request) {

    $node->setPublished(FALSE);
    $node->save();

    return $this->generateResponse();
  }

  /**
   * Generates a response to refrech a view after a conten has been updated.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   Refresh a view via Ajax.
   */
  protected function generateResponse() {
    // Refresh a view via Ajax.
    $response = new AjaxResponse();
    // Specify a view's selector for refreshing a view with Ajax.
    // It maybe a view's machine name, but underscores replaced with hyphens.
    // Also it can be a CSS class that you specified in 'ADVANCED' section
    // of a view's settings page.
    $selector = '.YOUR-VIEW-NAME';
    $method = 'triggerHandler';
    $arguments = ['RefreshView'];
    $response->addCommand(new InvokeCommand($selector, $method, $arguments));

    return $response;
  }

}
